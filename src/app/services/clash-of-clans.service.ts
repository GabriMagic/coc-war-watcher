import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { environment } from 'src/environments/environment';

import { Clan } from '../models/clan';
import { CurrentWar } from '../models/current-war';
import { GetClan } from '../ngrx/actions/get-clan.action';
import { GetCurrentWar } from '../ngrx/actions/get-current-war.action';
import { AppStore } from '../ngrx/store/app.store';

@Injectable({
  providedIn: 'root'
})
export class ClashOfClansService {

  constructor(
    private readonly http: HttpClient,
    private readonly store: Store<AppStore>
  ) {
    if (!!localStorage.getItem('clanTag')) {
      this.getClanInfo(localStorage.getItem('clanTag'));
    }
  }

  getClanInfo(tag: string): void {
    this.http.get<Clan>(`${environment.apiUrl}/clans/${encodeURIComponent(tag)}`).subscribe(res => {
      this.store.dispatch(new GetClan(res));
    });
  }

  getCurrentClanWar(tag: string): void {
    this.http.get<CurrentWar>(`${environment.apiUrl}/clans/${encodeURIComponent(tag)}/currentwar`).subscribe(res => {
      this.store.dispatch(new GetCurrentWar(res));
    });
  }
}
