import { TestBed } from '@angular/core/testing';

import { ClashOfClansService } from './clash-of-clans.service';

describe('ClashOfClansService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClashOfClansService = TestBed.get(ClashOfClansService);
    expect(service).toBeTruthy();
  });
});
