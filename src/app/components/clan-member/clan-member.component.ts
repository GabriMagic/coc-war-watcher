import { Component, Input } from '@angular/core';
import { ClanMember } from 'src/app/models/clan-member';

@Component({
  selector: 'app-clan-member',
  templateUrl: './clan-member.component.html',
  styleUrls: ['./clan-member.component.scss'],
})
export class ClanMemberComponent {

  @Input()
  member: ClanMember = new ClanMember();

}
