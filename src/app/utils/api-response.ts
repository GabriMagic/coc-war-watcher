import { Paging } from './paging';

export class ApiResponse<T> {
    items: T[];
    paging: Paging;
}
