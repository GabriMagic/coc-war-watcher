import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoadingController } from '@ionic/angular';
import { Store } from '@ngrx/store';
import { Clan } from 'src/app/models/clan';
import { AppStore } from 'src/app/ngrx/store/app.store';
import { ClashOfClansService } from 'src/app/services/clash-of-clans.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  clanForm: FormGroup;
  clan: Clan = new Clan();

  constructor(
    private readonly loadingController: LoadingController,
    private readonly formBuilder: FormBuilder,
    private readonly clashOfClansService: ClashOfClansService,
    private readonly store: Store<AppStore>
  ) { }

  ngOnInit(): void {
    this.store.select<Clan>('global', 'clan')
      .subscribe(clan => {
        this.clan = clan;
      });
    this.clanForm = this.formBuilder.group({
      clanTag: [localStorage.getItem('clanTag') ? localStorage.getItem('clanTag') : '',
      [Validators.required, Validators.pattern('^#[A-Z0-9]*$')]]
    });

    // Uppercase searchBar text
    this.clanForm.get('clanTag').valueChanges.subscribe(val => {
      this.clanForm.get('clanTag').setValue(val.toUpperCase(), {
        emitEvent: false,
        emitModelToViewChange: false
      });
    });
  }

  searchGuild(): void {
    this.clashOfClansService.getClanInfo(this.clanForm.value.clanTag);
    localStorage.setItem('clanTag', this.clanForm.value.clanTag);
  }
}
