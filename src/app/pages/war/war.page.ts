import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { CurrentWar } from 'src/app/models/current-war';
import { WarMember } from 'src/app/models/war-member';
import { AppStore } from 'src/app/ngrx/store/app.store';
import { ClashOfClansService } from 'src/app/services/clash-of-clans.service';

@Component({
  selector: 'app-war',
  templateUrl: './war.page.html',
  styleUrls: ['./war.page.scss'],
})
export class WarPage implements OnInit {

  currentWar: CurrentWar = new CurrentWar();

  constructor(
    private readonly clashOfClansService: ClashOfClansService,
    private readonly store: Store<AppStore>,
  ) { }

  ngOnInit() {
    this.store.select('global', 'currentWar')
      .subscribe(currentWar => {
        this.currentWar = currentWar;
      });

    const tag = localStorage.getItem('clanTag');
    this.clashOfClansService.getCurrentClanWar(tag);
  }

  orderedMembers(members: WarMember[]): WarMember[] {
    return members
      .sort((m1, m2) => m1.mapPosition > m2.mapPosition ? 1 : m1.mapPosition === m2.mapPosition ? 0 : -1);
  }
}
