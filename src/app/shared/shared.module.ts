import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { ClanMemberComponent } from '../components/clan-member/clan-member.component';

@NgModule({
  declarations: [
    ClanMemberComponent
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    ClanMemberComponent
  ]
})
export class SharedModule { }
