import { Clan } from 'src/app/models/clan';
import { CurrentWar } from 'src/app/models/current-war';

import { GlobalActions, GlobalActionTypes } from '../global.actions.types';
import { AppStore } from '../store/app.store';


const initialState: AppStore = {
    clan: new Clan(),
    currentWar: new CurrentWar()
};

export function globalReducer(state = initialState, action: GlobalActions): AppStore {
    switch (action.type) {
        case GlobalActionTypes.GetClan:
            return { ...state, clan: action.payload };
        case GlobalActionTypes.GetCurrentWar:
            return { ...state, currentWar: action.payload };
        default:
            return state;
    }
}
