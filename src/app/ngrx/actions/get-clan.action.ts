import { Action } from '@ngrx/store';
import { Clan } from 'src/app/models/clan';

import { GlobalActionTypes } from '../global.actions.types';

export class GetClan implements Action {
    readonly type = GlobalActionTypes.GetClan;
    constructor(public payload: Clan) { }
}
