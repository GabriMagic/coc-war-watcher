import { Action } from '@ngrx/store';
import { CurrentWar } from 'src/app/models/current-war';

import { GlobalActionTypes } from '../global.actions.types';

export class GetCurrentWar implements Action {
    readonly type = GlobalActionTypes.GetCurrentWar;
    constructor(public payload: CurrentWar) { }
}
