import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from 'src/environments/environment';

import { globalReducer } from './reducers/global.reducers';
import { AppStore } from './store/app.store';

export const rootReducers: ActionReducerMap<AppStore> = {
    global: globalReducer
};

export const metaReducers: MetaReducer<AppStore>[] = !environment.production ? [] : [];
