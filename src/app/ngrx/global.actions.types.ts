import { GetClan } from './actions/get-clan.action';
import { GetCurrentWar } from './actions/get-current-war.action';


export enum GlobalActionTypes {
    GetClan = '[Global] Get a list of clans',
    GetCurrentWar = '[Global] Get curent war of a clan'
}

export type GlobalActions = GetClan | GetCurrentWar;
