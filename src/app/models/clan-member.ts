import { League } from './league';

export class ClanMember {
    tag: string;
    name: string;
    role: string;
    expLevel: number;
    league: League;
    trophies: number;
    versusTrophies: number;
    clanRank: number;
    previousClanRank: number;
    donations: number;
    donationsReceived: number;
}
