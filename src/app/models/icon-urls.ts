export class IconUrls {
    small: string;
    tiny: string;
    medium: string;
}
