import { BadgeUrls } from './badge-urls';
import { ClanMember } from './clan-member';
import { Location } from './location';

export class Clan {
    tag: string;
    name: string;
    type: string;
    description: string;
    location: Location = new Location();
    badgeUrls: BadgeUrls = new BadgeUrls();
    clanLevel: number;
    clanPoints: number;
    clanVersusPoints: number;
    requiredTrophies: number;
    warFrequency: string;
    warWinStreak: number;
    warWins: number;
    warTies: number;
    warLosses: number;
    isWarLogPublic: boolean;
    members: number;
    memberList: ClanMember[] = [];
}

