import { WarClan } from './war-clan';

export class CurrentWar {
    state: string;
    teamSize: number;
    preparationStartTime: string;
    startTime: string;
    endTime: string;
    clan: WarClan = new WarClan();
    opponent: WarClan = new WarClan();
}
