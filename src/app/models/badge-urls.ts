export class BadgeUrls {
    small: string;
    large: string;
    medium: string;
}
