import { BadgeUrls } from './badge-urls';
import { WarMember } from './war-member';

export class WarClan {
    tag: string;
    name: string;
    badgeUrls: BadgeUrls = new BadgeUrls();
    clanLevel: number;
    attacks: number;
    stars: number;
    destructionPercentage: number;
    members: WarMember[] = [];
}
