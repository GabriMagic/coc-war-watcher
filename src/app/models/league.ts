import { IconUrls } from './icon-urls';

export class League {
    id: number;
    name: string;
    iconUrls: IconUrls;
}

