export class WarMember {
    tag: string;
    name: string;
    townhallLevel: number;
    mapPosition: number;
    opponentAttacks: number;
}
